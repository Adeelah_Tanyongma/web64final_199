const bcrypt =require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'travel',
    password : 'travel123',
    database : 'AttractionSystem'
});

connection.connect();

const express = require('express')
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/*Query 1 : List All Registrations
SELECT Reviewer.ReviewerID, 
       Reviewer.ReviewerName, 
       Reviewer.ReviewerSurname, 
       PlaceReview.PlaceName, 
       Registration.RegistrationTime
FROM Reviewer,Registration,PlaceReview
WHERE (Registration.ReviewerID = Reviewer.ReviewerID) AND 
      (Registration.PlaceID = PlaceReview.PlaceID)
*/
app.get("/list_registration", (req, res) => {
    let query = `SELECT Reviewer.ReviewerID, 
                        Reviewer.ReviewerName, 
                        Reviewer.ReviewerSurname, 
                        PlaceReview.PlaceName, 
                        Registration.RegistrationTime
                FROM Reviewer,Registration,PlaceReview
                WHERE (Registration.ReviewerID = Reviewer.ReviewerID) AND 
                      (Registration.PlaceID = PlaceReview.PlaceID)`;
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error querying from review db"
                        })
        }else {
            res.json(rows)
        }   
    });
})

/*Query 2 : List All Registrations by PlaceReview
SELECT Reviewer.ReviewerID, 
       Reviewer.ReviewerName, 
       Reviewer.ReviewerSurname, 
       PlaceReview.PlaceName, 
       Registration.RegistrationTime
FROM Reviewer,Registration,PlaceReview
WHERE (Registration.ReviewerID = Reviewer.ReviewerID) AND 
      (Registration.PlaceID = PlaceReview.PlaceID) AND
      (Registration.PlaceID = 1)
*/
app.get("/list_reg_place", (req, res) => {

    let place_id = req.query.place_id;

    let query = `SELECT Reviewer.ReviewerID, 
                        Reviewer.ReviewerName, 
                        Reviewer.ReviewerSurname, 
                        PlaceReview.PlaceName, 
                        Registration.RegistrationTime
                FROM Reviewer,Registration,PlaceReview
                WHERE (Registration.ReviewerID = Reviewer.ReviewerID) AND 
                      (Registration.PlaceID = PlaceReview.PlaceID) AND
                      (Registration.PlaceID = ${place_id})`;
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error querying from review db"
                        })
        }else {
            res.json(rows)
        }   
    });
})

/*Query 2 : List All Registrations by ReviewerID
SELECT Reviewer.ReviewerID, 
       PlaceReview.PlaceName, 
       Registration.RegistrationTime
FROM Reviewer,Registration,PlaceReview
WHERE (Reviewer.ReviewerID = Registration.ReviewerID) AND 
	  (Registration.PlaceID = PlaceReview.PlaceID) AND
      (Reviewer.ReviewerID = 1)
*/
app.get("/list_reg_reviewerid", authenticateToken, (req, res) => {
    let reviewer_id = req.user.user_id

    if (!req.user.IsAdmin) { res.send("Unauthorized Because you're not admin") }
    else {
        let query = `SELECT Reviewer.ReviewerID, 
                            PlaceReview.PlaceName, 
                            Registration.RegistrationTime
                    FROM Reviewer,Registration,PlaceReview
                    WHERE (Reviewer.ReviewerID = Registration.ReviewerID) AND 
                        (Registration.PlaceID = PlaceReview.PlaceID) AND
                        (Reviewer.ReviewerID = ${reviewer_id})`;
        connection.query( query, (err, rows) => {
            if(err) {
                res.json({ 
                            "status" : "400",
                            "message" : "Error querying from review db"
                            })
            }else {
                res.json(rows)
            }   
        });
    }
})

/* API for Registering a new Place Review */
app.post("/register_place", authenticateToken, (req, res) => {
    let user_profile = req.user
    let reviewer_id = req.user.user_id
    let place_id = req.query.place_id

    let query = `INSERT INTO Registration
                    (ReviewerID, PlaceID, RegistrationTime) 
                    VALUES ('${reviewer_id}',
                            '${place_id}',
                             NOW() )`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "static" : "400",
                        "message" : "Error inserting data into db"               
                    })
        }else {
            res.json({
                "static" : "200",
                "message" : "Adding event succesful"

            })
        }
    });

});

/* API for Processing Reviewer Authorization */
app.post("/login", (req, res)=> {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * 
                FROM Reviewer 
                WHERE Username='${username}'`

                connection.query( query, (err, rows) => {
                    if(err) {
                        res.json({ 
                                    "status" : "400",
                                    "message" : "Error querying from review db"
                                    })
                    }else {
                        let db_password = rows[0].Password
                        bcrypt.compare(user_password, db_password, (err, result)=>{
                            if (result){
                                let payload = {
                                    "username" : rows[0].Username,
                                    "user_id" : rows[0].ReviewerID,
                                    "IsAdmin" : rows[0].IsAdmin
                                }
                                console.log(payload)
                                let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                                res.send(token)
                            }else { res.send("Invalid username / password")}
                        })
                    }   
                });
            })
            

/* API for Registering a new Reviewer */
app.post("/register_reviewer", (req, res) => {

    let reviewer_name = req.query.reviewer_name
    let reviewer_surname = req.query.reviewer_surname
    let reviewer_username = req.query.reviewer_username 
    let reviewer_password = req.query.reviewer_password

    bcrypt.hash(reviewer_password, SALT_ROUNDS,(err, hash)=> {
                let query = `INSERT INTO Reviewer
                            (ReviewerName, ReviewerSurname, Username, Password, IsAdmin) 
                            VALUES ('${reviewer_name}',
                                    '${reviewer_surname}',
                                    '${reviewer_username}',
                                    '${hash}',false)`
                console.log(query)

                connection.query( query, (err, rows) => {
                 if(err) {
                res.json({ 
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
                 }else {
                res.json({
                        "status" : "200",
                        "message" : "Adding new user succesful"
                })
                 }    
             });
        })
});

/* CRUD Operation for PlaceReview Table */
app.get("/list_place", (req, res) => {
    let query = "SELECT * FROM PlaceReview";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error querying from review db"
                        })
        }else {
            res.json(rows)
        }   
    });
})

/* Add Place */
app.post("/add_place", (req, res) => {

    let place_name = req.query.place_name
    let place_location = req.query.place_location
    let place_activity = req.query.place_activity

    let query = `INSERT INTO PlaceReview
                (PlaceName, PlaceLocation, PlaceActivity) 
                VALUES ('${place_name}','${place_location}','${place_activity}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }    
    });

})

/* Update Place */
app.post("/update_place", (req, res) => {

    let place_id = req.query.place_id
    let place_name = req.query.place_name
    let place_location = req.query.place_location
    let place_activity = req.query.place_activity

    let query = `UPDATE PlaceReview SET
                    PlaceName = '${place_name}',
                    PlaceLocation = '${place_location}',
                    PlaceActivity = '${place_activity}'
                    WHERE PlaceID = ${place_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating place succesful"
            })
        }    
    });

})

/* Delete Place */
app.post("/delete_place", (req, res) => {

    let place_id = req.query.place_id

    let query = `DELETE FROM PlaceReview WHERE PlaceID = ${place_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record success"
            })
        }    
    });

})

app.listen(port, () => {
    console.log(`Now starting Place Review  Backend ${port} `)
})


/*query = "SELECT * from Reviewer";
connection.query( query, (err, rows) => {
        if(err) {
            console.log(err);
        }else {
            console.log(rows);
        }
});

connection.end();*/