-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 04, 2022 at 12:52 AM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `AttractionSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `PlaceReview`
--

CREATE TABLE `PlaceReview` (
  `PlaceID` int(11) NOT NULL,
  `PlaceName` varchar(200) NOT NULL,
  `PlaceLocation` varchar(300) NOT NULL,
  `PlaceActivity` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PlaceReview`
--

INSERT INTO `PlaceReview` (`PlaceID`, `PlaceName`, `PlaceLocation`, `PlaceActivity`) VALUES
(1, 'แหลมกระทิง', 'ภูเก็ต', 'จุดชมวิวของภูเก็ต ที่มองเห็นทะเลอันดามันเกือบ 360 องศา สำหรับการดูวิวที่แหลมกระทิง แนะนำว่าควรมาในช่วงบ่ายๆ คุณจะมองวิวสวยตามข้างชนิดที่ต้องร้องว๊าวกันเลยทีเดียว แต่ทั้งไม่ควรมาช่วงหลังพระอาทิตย์ตก เพราะจะเดินทางลำบากและทางค่อนข้างอันตราย'),
(2, 'เสม็ดนางชี', 'พังงา', 'แหล่งท่องเที่ยวธรรมชาติอันสวยงาม ซึ่งถือว่าเป็นจุดชมวิวที่เป็นสวรรค์แห่งอ่าวพังงาเลยก็ว่าได้ ที่แห่งนี้จะทำให้คุณได้สัมผัสบรรยากาศของภูเขาและทะเลอันสวยงาม ท่ามกลางบรรยากาศอันเงียบสงบ สุดผ่อนคลาย ไปพร้อมๆกัน ที่สร้างความสุขให้คุณแบบสุดๆไปเลย'),
(3, 'ถ้ำภูผาเพชร', 'จังหวัดสตูล', 'ถ้ำภูผาเพชร เป็นถ้ำที่มีขนาดใหญ่ติดอันดับ 4 ของโลก ตั้งอยู่ในอำเภอมะนัง ภายในถ้ำจะพบกับห้องโถง เพดานสูง ที่เต็มไปด้วยหินงอกหินย้อยที่ยังคงมีหยดน้้ำเกาะตัวอยู่ เมื่อกระทบกับแสงไฟจะเห็นความงามของแสงที่ส่องประกาย นอกจากนี้ยังห้องที่แบ่งตามลักษณะของธรณีสัณฐานที่พบเป็น 20 ห้อง อาทิ ห้องม่านเพชร ที่มีลักษณะคล้ายผ้าม่านแขวนอยู่ , ห้องพญานาค ที่มีลักษณะเป็นหินงอกคล้ายงูใหญ่หรือพญานาค เป็นต้น สำหรับนักท่องเที่ยวที่ต้องการเข้าชม ควรแต่งกายให้พร้อม สวมรองเท้าที่สะดวกสบาย และพกไฟฉายติดตัวไปด้วย'),
(4, 'เกาะเขาใหญ่', 'สตูล', 'สถานที่ท่องเที่ยวฝั่งอันดามัน ที่ไม่ควรพลาดอีกแห่งหนึ่ง เกาะเขาใหญ่ เป็นเกาะหินปูนกลางทะเล อยู่ในอุทยานแห่งชาติหมู่เกาะเภตรา ตำบลปากน้ำ อำเภอละงู จังหวัดสตูล โดยเกาะแห่งนี้มีชายหาดสวยงามที่เงียบสงบ ที่ชื่อว่า \"นะปุลา\" แถมยังมีไฮไลด์ที่ ปราสาทหินพันยอด สิ่งอัศจรรย์สุดอันซีนที่ธรรมชาติรังสรรค์ขึ้นจากการกัดเซาะหินของน้ำฝน จนกลายเป็นแท่งหินแหลมรูปร่างสวยงามแปลกตาคล้ายกับปราสาทในเทพนิยาย โดยสามารถลอดเข้าไปชมในเวลาที่น้ำลด'),
(5, 'เมืองเก่าสงขลา', 'สงขลา', 'เมืองเก่าสงขลา ตั้งอยู่ในเขตอำเภอเมือง ที่ยังคงเอกลักษณ์ดั้งเดิมไว้ เป็นสถานที่สำคัญของเมืองให้ได้เรียนรู้ประวัติศาสตร์ พร้อมทั้งมีงานสตรีทอาร์ตเป็นภาพวาดอยู่บนกำแพง ที่สร้างความมีชีวิตชีวาน่าดึงดูดให้มาเที่ยวชมในเมืองเก่าแห่งนี้ นอกจากนี้ยังมีร้านของกินอร่อยๆให้เลือกกินอร่อยๆเพียบ เหมาะสำหรับการมาเช็กอินเป็นอย่างมาก'),
(6, 'อุทยานแห่งชาติหมู่เกาะอ่างทอง', 'สุราษฎร์ธานี', 'ดินแดนแห่งหมู่เกาะที่จะทำให้คุณได้เพลิดเพลินไปกับภูเขาเขียวขจีอันสวยงาม พร้อมกับท้องทะเลที่เต็มไปด้วยหมู่เกาะสวยงามต่างๆ และภาพทิวทัศน์ดึงดูดตา ท่ามกลางบรรยากาศเย็นสบายๆ นอกจากนี้ยังมีไฮไลด์คือ เป็นที่ตั้งจุดชมวิวที่สวยที่สุด เหมาะกับการนั่งชิลรับอากาศบริสุทธิ์ของท้องฟ้าและน้ำทะเลในวันหยุดสุดสบายที่สุด');

-- --------------------------------------------------------

--
-- Table structure for table `Registration`
--

CREATE TABLE `Registration` (
  `RegistrationID` int(11) NOT NULL,
  `ReviewerID` int(11) NOT NULL,
  `PlaceID` int(11) NOT NULL,
  `RegistrationTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Registration`
--

INSERT INTO `Registration` (`RegistrationID`, `ReviewerID`, `PlaceID`, `RegistrationTime`) VALUES
(1, 1, 1, '2022-04-03 16:41:09'),
(2, 2, 2, '2022-04-03 18:50:57'),
(3, 3, 3, '2022-04-03 18:52:55'),
(4, 4, 4, '2022-04-03 18:53:00'),
(5, 5, 5, '2022-04-03 18:53:06'),
(6, 6, 6, '2022-04-03 18:53:11'),
(7, 8, 1, '2022-04-04 00:08:49'),
(9, 8, 2, '2022-04-04 00:10:17'),
(10, 7, 2, '2022-04-04 00:13:54');

-- --------------------------------------------------------

--
-- Table structure for table `Reviewer`
--

CREATE TABLE `Reviewer` (
  `ReviewerID` int(11) NOT NULL,
  `ReviewerName` varchar(50) NOT NULL,
  `ReviewerSurname` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Reviewer`
--

INSERT INTO `Reviewer` (`ReviewerID`, `ReviewerName`, `ReviewerSurname`, `Username`, `Password`, `IsAdmin`) VALUES
(1, 'Afnan', 'Sama', '', '', 0),
(2, 'Amni', 'Jaya', '', '', 0),
(3, 'Aman', 'Hayi', '', '', 0),
(4, 'Asma', 'Yosoh', '', '', 0),
(5, 'Adeelah', 'Tanyongma', '', '', 0),
(6, 'Anda', 'Dalif', '', '', 0),
(7, 'Aman', 'Damai', 'Aman.damai', '$2b$10$C0cktHpbqmwvLlyXDTRVueP8C4imtxPPD4Rg4ePDNglDu75PEdKKi', 1),
(8, 'Ananda', 'Ulya', 'Ananda.a', '$2b$10$uf0oQoPSb5MgEEpQQuhHdeBQFGMGBwHhfSK8hhBx/H1obc3tNcary', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `PlaceReview`
--
ALTER TABLE `PlaceReview`
  ADD PRIMARY KEY (`PlaceID`);

--
-- Indexes for table `Registration`
--
ALTER TABLE `Registration`
  ADD PRIMARY KEY (`RegistrationID`),
  ADD KEY `ReviewerID` (`ReviewerID`),
  ADD KEY `PlaceID` (`PlaceID`);

--
-- Indexes for table `Reviewer`
--
ALTER TABLE `Reviewer`
  ADD PRIMARY KEY (`ReviewerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `PlaceReview`
--
ALTER TABLE `PlaceReview`
  MODIFY `PlaceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Registration`
--
ALTER TABLE `Registration`
  MODIFY `RegistrationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Reviewer`
--
ALTER TABLE `Reviewer`
  MODIFY `ReviewerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Registration`
--
ALTER TABLE `Registration`
  ADD CONSTRAINT `PlaceID` FOREIGN KEY (`PlaceID`) REFERENCES `PlaceReview` (`PlaceID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ReviewerID` FOREIGN KEY (`ReviewerID`) REFERENCES `Reviewer` (`ReviewerID`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
